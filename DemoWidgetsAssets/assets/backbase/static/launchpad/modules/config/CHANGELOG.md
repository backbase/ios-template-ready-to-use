### v 1.0.0
* Initial release
## [2.0.0] - 2015-05-12 (note: generated from git logs)

 - LPES-3657: i18n: added sk-SK
 - add angular ad AMD module from window
 - LPES-0000 i18n: negative currency format for en-US changed to "-¤xx.xx"
 - LPES-0000 i18n: added sk-SK
 - Make requirejs config decide automatically which module path to use based on the environment.
