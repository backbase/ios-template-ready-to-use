## [2.0.0] - 2015-05-12 (note: generated from git logs)

 - add setconfig getconfig in api add module-payments as bower deps
 - ignore  .bower.json
 - EBANK-217 Add missing style files
 - EBANK-217 Create components to be able to schedule a payment
