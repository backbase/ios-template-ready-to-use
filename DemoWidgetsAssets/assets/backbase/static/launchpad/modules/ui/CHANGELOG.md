### v 0.0.0
* Initial release
## [2.0.0] - 2015-05-12 (note: generated from git logs)

 - LPES-3157: Account number doesn't change when switching between accounts
 - EBANK-255 Support multiple wizard directives in one page
 - EBANK-255 Update indentation to spaces
 - EBANK-217 Move EndOn under payment.scheduledTransfer
 - Build regenerated.
 - Meta info files for SourceJS to be able to render UI components with proper navigation structure.
 - Minor fix of button rendering order of card component.
 - EBANK-234 Add endOn field to paymentOrder to be able to use selected value
 - NOJIRA: Move stripHtml string check to the method
 - UI component names clean up and unification. Tests clean up.
 - NOJIRA: fix smartsuggest falsy values
 - Added the demo for color-picker component. Added link into index.html for standalone development. Readme updated.
 - NOJIRA: remove window.lp.utils dependency from smartsuggest component
