# Launchpad Core :: Session Module

# Information
| name                  | version       | bundle     |
| ----------------------|:-------------:| ----------:|
| core.session          | 1.0.0         | launchpad  |

## Dependencies
* [base][base-url]


[base-url]:http://stash.backbase.com:7990/projects/lpm/repos/foundation-base/browse/
[core-url]: http://stash.backbase.com:7990/projects/lpm/repos/foundation-core/browse/
[ui-url]: http://stash.backbase.com:7990/projects/lpm/repos/ui/browse/
[config-url]: https://stash.backbase.com/projects/LP/repos/config/browse
[api-url]:http://stash.backbase.com:7990/projects/LPM/repos/api/browse/
