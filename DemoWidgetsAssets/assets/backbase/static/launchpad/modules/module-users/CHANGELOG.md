### v 1.0.0
* Initial release
## [2.0.0] - 2015-05-12 (note: generated from git logs)

 - LPES-3536: Remove unused locationProvider.
 - NOJIRA: add 403 error when security risk in otp login
 - LPES-3568: handle disconnections and show nice 500 error message
 - Added testing for isVerified on authentication API.
 - Change status check functions to be case insensitive.
 - remove console
 - use dist
 - add setConfig and getConfig in API
 - ignore .bower.json
