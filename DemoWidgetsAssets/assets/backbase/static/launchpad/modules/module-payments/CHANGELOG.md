## [2.0.0] - 2015-05-12 (note: generated from git logs)

 - LPES-3664: disable currency selection issue
 - Merge from master
 - LPES-3539: no IBAN issue
 - LPES-3539: revert data changes
 - LPES-3539: filling issue - 3
 - LPES-3539: filling issue - 2
 - LPES-3539: filling issue
 - Add setConfig & get Config to provider API
 - EBANK-217 Move EndOn under payment.scheduledTransfer
 - LPES-3539: del edit
 - EBANK-234 Add endOn field to paymentOrder to be able to use selected value
 - EBANK-234 Remove scope as it breaks existing component usages
 - EBANK-238 Remove unneeded comma
 - EBANK-234 Add ability to pass in custom frequency and end options lists to a component
