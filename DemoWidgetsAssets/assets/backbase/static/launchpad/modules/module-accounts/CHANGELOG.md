## [2.0.0] - 2015-05-12 (note: generated from git logs)

 - LPES-3670: locale BBAN en-US
 - LPES-3637: get page preference
 - LPES-3637: bubbling properties getter
 - LPES-3637: account masking in dropdown
 - LPES-3556: added the dist.
 - LPES-3556: Fixes a potential bug with assets load function being resolved too quickly.
